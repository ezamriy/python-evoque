%global pkg     evoque

Name:           python-evoque
Version:        0.4
Release:        1%{?dist}
Summary:        Managed eval-based freeform templating for python

Packager:       Eugene Zamriy <eugene@zamriy.info>
Vendor:         Mario Ruggier <mario@ruggier.org>

Group:          Development/Languages
License:        AFL
URL:            http://%{pkg}.gizmojo.org/
Source0:        http://gizmojo.org/dist/%{pkg}-%{version}.tar.gz

BuildArch:      noarch

Provides:       evoque

BuildRequires:  python-setuptools


%description
Evoque is a full-featured generic text templating system for python.


%prep
%setup -q -n %{pkg}-%{version}


%build
%{__python} setup.py build


%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}


%clean
%{__rm} -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc CHANGES.txt README.txt
%{python_sitelib}/*


%changelog

* Sat Aug  3 2013 Eugene G. Zamriy <eugene@zamriy.info> - 0.4-1
- Initial release. 0.4 version
